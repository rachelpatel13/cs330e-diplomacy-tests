#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_print

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
   
    # ----
    # diplomacy_read
    # ----

    def test_read_1(self):
        input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        output = diplomacy_read(input)
        self.assertEqual(output, {'Hold': [['A', 'Madrid']], 'Move': [['B', 'Barcelona', 'Madrid']], 'Support': [['C', 'London', 'B']]})

    # ----
    # diplomacy_solve
    # ----

    def test_solve_1(self):
        input = StringIO('A Madrid Support B\nB Barcelona Support A\n')
        output = StringIO('')
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(), 'A Madrid\nB Barcelona\n')


    def test_solve_2(self):
        input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\nD Paris Support C\n')
        output = StringIO('')
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(),'A [dead]\nB [dead]\nC Barcelona\nD Paris\n')

    def test_solve_3(self):
        input = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Support B\nE Dallas Move Paris\nF Beijing Support D\n')
        output = StringIO('')
        diplomacy_solve(input, output)
        self.assertEqual(output.getvalue(),'A Madrid\nB [dead]\nC London\nD Paris\nE [dead]\nF Beijing\n')

    # -----
    # diplomacy_print
    # -----

    # def test_print_1(self):
        # pass


# ----
# main
# ----

if __name__ == "__main__":
    main()


""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.000s

% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.000s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          93      0     36      0   100%
TestDiplomacy.py      25      0      2      1    96%   71->exit
--------------------------------------------------------------
TOTAL                118      0     38      1    99%
"""